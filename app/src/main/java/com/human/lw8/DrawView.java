package com.human.lw8;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

/**
 * Created by human on 15.04.18.
 */

public class DrawView extends View {

    Paint paint;
    float x,y;



    public DrawView(Context context) {
        super(context);
        paint = new Paint();

        try {

            Display disp = ((WindowManager)this.getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            x = disp.getWidth()/2;
            y = disp.getHeight()/2;
        }
        catch (Exception e){
            Log.i("message", e.getMessage());
        }
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int n = SecondActivity.seekBarValue;
        for (int i = 0; i < n; i++)
        canvas.drawCircle(x, y, 10 + i*10, paint);

    }
}
